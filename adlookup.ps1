while($true){
    Write-Host "CitiDomain User Lookup" 
	$user = read-host -prompt 'User'
	clear
	$return = get-aduser -server citidomain.cititrends.com $user -properties * | Select-Object -property CN, SamAccountName, @{l='OU';e={$_.DistinguishedName.split(',')[1].split('=')[1]}}, Description, Title, LockedOut, Enabled, PasswordExpired| Format-List
	$return
}